BRAND OBSERVER - FALCOR
=======================

**Falcor** is a web scraper and spider collecting data on brands from open sources such as Wikipedia.

![Falcor - The Neverending Story](http://media2.giphy.com/media/DPKp5TwMFnIcg/giphy.gif)