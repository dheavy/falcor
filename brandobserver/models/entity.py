# -*- coding: utf-8 -*-

class Entity:

  name                = None
  logo_url            = None
  creation_date       = None
  key_dates           = None
  founders            = None

  corporate_structure = None
  signature           = None
  headquarters        = None
  director            = None
  shareholders        = None
  business_activity   = None
  subsidiaries        = None
  workforce           = None
  siren_number        = None
  website             = None
  capitalization      = None
  turnover            = None
  net_result          = None

  def __init__(self, data):
    for key, value in data.iteritems():
      try:
        setattr(self, key, value)
      except AttributeError:
        pass