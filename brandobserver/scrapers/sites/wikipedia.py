# -*- coding: utf-8 -*-
import re, sys, requests
from bs4 import BeautifulSoup
from requests.exceptions import ConnectionError
from brandobserver.scrapers.sites.scraper import Scraper

class Wikipedia(Scraper):
  """The scraper for infoboxes in Wikipedia pages."""

  ROOT_URL = 'http://en.wikipedia.org/'

  data = {}

  def parse(self, lang='en'):
    """Parse the infobox from the Wikipedia page.

    Returns:
      A dictionary with fields of data from the infobox.

    """
    if lang == 'en':

      infobox = self.markup.select('.infobox')[0]
      self.data = {
        u'name':             self._safe_scrape(infobox, '.fn.org', 0).text,
        u'logo_url':         u''.join(self._safe_scrape(infobox, 'a.image img', 0)['src']),
        u'type':             self._safe_scrape(infobox, 'tr td', 1).text,
        u'traded_as':        self._safe_scrape(infobox, 'tr td', 2).text,
        u'industry':         self._safe_scrape(infobox, 'tr td', 3).text,
        u'founded':          self._safe_scrape(infobox, 'tr td', 4).text,
        u'founders':         self._safe_scrape(infobox, 'tr td', 5).text,
        u'headquarters':     self._safe_scrape(infobox, 'tr td', 6).text,
        u'key_people':       self._safe_scrape(infobox, 'tr td', 7).text,
        u'products':         self._safe_scrape(infobox, 'tr td', 8).text.strip().replace('\n', ', '),
        u'revenue':          self._safe_scrape(infobox, 'tr td', 9).text,
        u'operating_income': self._safe_scrape(infobox, 'tr td', 10).text,
        u'profit':           self._safe_scrape(infobox, 'tr td', 11).text,
        u'total_assets':     self._safe_scrape(infobox, 'tr td', 12).text,
        u'total_equity':     self._safe_scrape(infobox, 'tr td', 13).text,
        u'num_employees':    self._safe_scrape(infobox, 'tr td', 14).text,
        u'subsidiaries':     self.parse_subsidiaries(self._safe_scrape(infobox, 'tr td', 15).select('li')),
        u'website':          u''.join(self._safe_scrape(infobox, 'tr td', 16).select('a')[0]['href'])
      }
      return self.get_french_content()

    else:

      infobox = self.markup.select('.infobox_v3')[0]
      key_facts = infobox.select('table')[1]
      financial_data = infobox.select('table')[2]

      self.data[u'signature'] = key_facts('td')[1].text
      self.data[u'key_dates'] = infobox.select('table td')[1].text.strip().replace('\n', '').split(',')

      return self._cleanup(self.data)


  def parse_subsidiaries(self, node):
    """From given DOM node in markup, parse subsidiaries info, if any.

    Args:
      node: The DOM node from the markup where the subsidiaries should be found.

    Returns:
      A list of subsidiaries in the form of dictionaries with `href` and `name` entries.
      If `href` is not None, it is the link the Wikipedia page of the related subsidiary.

    """
    subsidiaries = []
    raw_subsidiaries = str(node).replace('<li>', '').replace('</li>', '').split(',')

    for raw_subsidiary in raw_subsidiaries:
      matches = re.search('href="\s*(.*?)\s*"', raw_subsidiary)

      if matches != None and matches.group(1) != None:
        name = re.search('<a\s*(.*?)\s*>\s*(.*?)\s*<\/a>', raw_subsidiary).group(2)
        subsidiaries.append({ 'name': name, 'href': self.ROOT_URL + matches.group(1) })
      else:
        name = raw_subsidiary.strip()
        subsidiaries.append({ 'name': name, 'href': None })

    return subsidiaries


  def get_french_content(self):
    """Scrape the French version of the page to complete data.

    Returns:
      The dictionary of data scraped.

    """
    fr_url = self.url.replace('en.', 'fr.')

    try:
      res = requests.get(fr_url)
    except ConnectionError:
      print '[ FALCOR ] Error... I could not reach `' + fr_url + '`. Please try again...'
      sys.exit()

    self.markup = BeautifulSoup(res.text.encode('utf-8'), 'lxml')

    return self.parse('fr')


  def _safe_scrape(self, markup, target, index):
    """Process safely the scraping of a node element.

    Args:
      markup: The node element from BeautifulSoup to process.
      target: The CSS class or id to target.
      index:  The index from the returned value of `markup.select()` to pick.

    Returns:
      Either the DOM element found, or None if an error occured (i.e. nothing is found).

    """
    try:
      return markup.select(target)[index]
    except IndexError:
      return None


  def _cleanup(self, data):
    """Proceed with last clean up before delivering data.

    Args:
      data: The dictionary of data to clean up.

    Returns:
      The clean dictionary.

    """
    for key, value in data.iteritems():
      if value[-3:-2] == '[':
        data[key] = value[0:-3]

    return data