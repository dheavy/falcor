# -*- coding: utf-8 -*-
import sys, time, requests
from requests.exceptions import ConnectionError
from bs4 import BeautifulSoup


class Scraper:
  """The base Scraper class all other scrapers should subclass.

  Contains the `scrape` method to start scraping, and the stubbed
  `parse` method the scrapers should implement on their end.

  """

  url    = None
  markup = None

  def __init__(self, url):
    self.url = url

  def scrape(self):
    """Scrape the URL and store markup for BeautifulSoup to process.

    Then invoke the `parse` method, a stub to be implemented in each
    subclass of the current class.

    Returns:
      The return value of the `parse` method.

    """
    try:
      res = requests.get(self.url)
    except ConnectionError:
      print '[ FALCOR ] Error... I could not reach `' + self.url + '`. Please try again...'
      sys.exit()

    self.markup = BeautifulSoup(res.text.encode('utf-8'), 'lxml')
    return self.parse()

  def parse(self):
    """Stubbed parsing method to be implemented by each Scraper's subclasses.

    Returns:
      Depends on the implementation. Ideally, an instance of `brandoserver.models.Entity`.

    """
    pass

  def dispose(self):
    """Free resources used by this instance. Primarly via decomposition of the markup object."""
    try:
      self.markup.decompose()
    except Error:
      pass
