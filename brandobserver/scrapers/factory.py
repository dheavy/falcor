# -*- coding: utf-8 -*-
import imp, os, errno, sys
from urlparse import urlparse

def create_from(url):
  """Create a new instance of a scraper from the given url.

  Args:
    url: The URL to scrape. The proper scraper, if it exists, will be generated from the URL's domain name.

  Returns:
    An instance of the matching scraper, if any matches. None otherwise.

  """

  # The variable storing the class instance we're expecting to return.
  class_inst = None

  # Get domain name.
  name = extract_name(url)

  # Capitalize the name passed as argument, to (hopfully) obtain the name
  # of the class we're expecting to instantiate, and build the file path
  # to its file.
  expected_class = name.capitalize()
  filepath = os.path.dirname(os.path.realpath(__file__)) + '/sites/' + name + '.py'

  # Unpack module name into a variable.
  mod_name, _ = os.path.splitext(os.path.split(filepath)[-1])

  # Artifically load module and reference it in a variable.
  try:
    py_mod = imp.load_source(mod_name, filepath)
  except EnvironmentError as e:
    return None

  # If loaded module has expected class, artifically
  # load a class instance with expected `url` argument in it.
  if hasattr(py_mod, expected_class):
    class_inst = getattr(py_mod, expected_class)(url)

  return class_inst

def extract_name(url):
  """Extract domain name from URL.

  Args:
    url: The URL to extract domain name from.

  Returns:
    str  The domain name.

  """
  parsed_url = urlparse(url)
  netloc = parsed_url.netloc
  domain_name = netloc[:netloc.rfind('.')]
  if domain_name.find('.') != -1:
    domain_name = domain_name[domain_name.rfind('.') + 1:]

  return domain_name